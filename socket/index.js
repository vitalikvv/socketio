import * as config from "./config";
import {texts} from "../data";
let activeUsers = [];
let activeRoomsWithUsers = {};
let roomsWithGamesInProcess = {};
let usersWithoutRooms = [];

export default io => {
  const setUsersProgress = (currentUser, userProgress, roomId) => {
    let usersInRoomProcess;
    for (let key in roomsWithGamesInProcess) {
      if (key === roomId) {
        const usersInRoom = roomsWithGamesInProcess[key];
        usersInRoomProcess = usersInRoom.map(user => {
          for (let key in user) {
            if(user.hasOwnProperty(key)){
              if (key === currentUser) {
                return { [key]: userProgress };
              }
            }
          }
          return user
        })
        roomsWithGamesInProcess[key] = usersInRoomProcess;
      }
    }
  }

  const checkIsTimerCanStart = (usersInRoom, roomId) => {
    let countOfReadyUsers = 0;
    usersInRoom.forEach(user => {
      for (let key in user) {
        if(user.hasOwnProperty(key)){
          if (user[key] === 'ready') {
            countOfReadyUsers++
          }
        }
      }
    })
    if (countOfReadyUsers === usersInRoom.length) {
      io.to(roomId).emit('ACTIVATED_TIMER', { seconds: config.SECONDS_TIMER_BEFORE_START_GAME, roomId });
      for (let key in activeRoomsWithUsers) {
        if (key === roomId) {
          roomsWithGamesInProcess[roomId] = activeRoomsWithUsers[key];
          delete activeRoomsWithUsers[key];
        }
      }
      usersWithoutRooms.map(socketId => {
        io.to(socketId).emit('UPDATE_ROOMS', activeRoomsWithUsers);
      })
    }
  }


  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    if (username) {
      if (activeUsers.indexOf(username) > -1) {
        socket.emit("REGISTRATION_ERROR", "User with such name existed");
        return;
      }
      activeUsers.push(username);
      usersWithoutRooms.push(socket.id);
    }

    socket.emit('SET_CURRENT_USER', username)
    socket.emit('UPDATE_ROOMS', activeRoomsWithUsers)

    socket.on('CREATE_ROOM', (roomId) => {
      if (roomId) {
        for (let key in activeRoomsWithUsers) {
          if (key === roomId) {
            socket.emit('ROOM_NAME_ERROR', 'Room with such name existed');
            return
          }
        }
        const userWithReady = {
          [username]: 'notReady'
        }
        activeRoomsWithUsers[roomId] = [userWithReady];
        const index = usersWithoutRooms.indexOf(socket.id);
        if (index > -1) {
          usersWithoutRooms.splice(index, 1);
        }
        socket.join(roomId)
        socket.emit('UPDATE_ROOM', {activeRoomsWithUsers, roomId});
        usersWithoutRooms.map(socketId => {
          io.to(socketId).emit('UPDATE_ROOMS', activeRoomsWithUsers);
        })
        socket.emit('SET_READY_BUTTON', {username, roomId});
        socket.emit('SAY_HELLO_INTRODUCE', username);
      }
    })

    socket.on('JOIN_ROOM', (payload) => {
      const { roomName:roomId, currentUser } = payload;
      let currentUsers
      for (let key in activeRoomsWithUsers) {
        if (key === roomId) {
          currentUsers = activeRoomsWithUsers[key]
        }
      }
      if (currentUsers.length >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit('JOIN_ROOM_ERROR', 'This room is full')
        return;
      }
      const userWithReady = {
        [username]: 'notReady'
      }
      currentUsers.push(userWithReady)
      socket.join(roomId, () => {
        activeRoomsWithUsers[roomId] = currentUsers;
        const index = usersWithoutRooms.indexOf(socket.id);
        if (index > -1) {
          usersWithoutRooms.splice(index, 1)
        }
        io.to(roomId).emit('UPDATE_ROOM', {activeRoomsWithUsers, roomId});
        io.to(roomId).emit('SAY_HELLO_INTRODUCE', currentUser);
        usersWithoutRooms.map(socketId => {
          io.to(socketId).emit('UPDATE_ROOMS', activeRoomsWithUsers);
        })
        socket.emit('SET_READY_BUTTON', {username, roomId});
      })
    })

    socket.on('BACK_TO_ROOMS', (payload) => {
      const { roomId, currentUser } = payload;
      let newUsersInRoom;
      for (let key in activeRoomsWithUsers) {
        if (key === roomId) {
          const usersInRoom = activeRoomsWithUsers[key];
          newUsersInRoom = usersInRoom.filter(user => {
            for (let key in user) {
              if(user.hasOwnProperty(key)){
                if (key === currentUser) {
                  return false
                }
              }
            }
          return true
          })
          if (newUsersInRoom.length > 0) {
            activeRoomsWithUsers[key] = newUsersInRoom;
          } else {
            delete activeRoomsWithUsers[key]
          }
        }
      }
      socket.leave(roomId)
      usersWithoutRooms.push(socket.id);
      socket.emit('UPDATE_ROOMS', activeRoomsWithUsers)
      usersWithoutRooms.map(socketId => {
        io.to(socketId).emit('UPDATE_ROOMS', activeRoomsWithUsers);
      })
      socket.broadcast.to(roomId).emit('UPDATE_ROOM', {activeRoomsWithUsers, roomId});
      checkIsTimerCanStart(newUsersInRoom, roomId);
    })

    socket.on('READY_USER', (payload) => {
      const { username, roomId, actionType } = payload
      let newUsersInRoom;
      for (let key in activeRoomsWithUsers) {
        if (key === roomId) {
          const usersInRoom = activeRoomsWithUsers[key];
          newUsersInRoom = usersInRoom.map(user => {
            for (let key in user) {
              if(user.hasOwnProperty(key)){
                if (key === username) {
                  return { [key]: actionType };
                }
              }
            }
            return user;
          })
          activeRoomsWithUsers[key] = newUsersInRoom;
        }
      }
      io.to(roomId).emit('UPDATE_ROOM', {activeRoomsWithUsers, roomId});
      checkIsTimerCanStart(newUsersInRoom, roomId)
    });

    socket.on('START_GAME', (payload) => {
      const {roomId, currentUser} = payload;
      const positionIdOfRandomText = Math.floor(Math.random()*texts.length);
      io.to(roomId).emit('REQUEST_FOR_RANDOM_TEXT', {roomId, positionIdOfRandomText, seconds: config.SECONDS_FOR_GAME});
      socket.emit('KEY_LISTENER', roomId)
      const userProgressInPCT = 0;
      setUsersProgress (currentUser, userProgressInPCT, roomId);
    });

    socket.on('GAME_PROGRESS', (payload) => {
      const { currentUser, userProgressInPCT, roomId } = payload;
      setUsersProgress (currentUser, userProgressInPCT, roomId);
      io.to(roomId).emit('UPDATE_PROGRESS_INDICATORS', {roomId, roomsWithGamesInProcess});
    });

    socket.on('USER_FINISHED', (payload) => {
      const { currentUser, finishTime, roomId } = payload;
      const userProgress = finishTime * -1;
      setUsersProgress (currentUser, userProgress, roomId);
      io.to(roomId).emit('UPDATE_PROGRESS_INDICATORS', {roomId, roomsWithGamesInProcess});
      // check is all users finished
      let usersInRoomProcess;
      let countOfFinishedUsers = 0;
      for (let key in roomsWithGamesInProcess) {
        if (key === roomId) {
          const usersInRoom = roomsWithGamesInProcess[key];
          usersInRoomProcess = usersInRoom.map(user => {
            for (let key in user) {
              if(user.hasOwnProperty(key)){
                if (user[key] < 0) {
                  countOfFinishedUsers++
                }
              }
            }
            return user;
          })
          if (usersInRoom.length === countOfFinishedUsers) {
            io.to(roomId).emit('PUBLISHING_RESULTS', {roomId, roomsWithGamesInProcess});
          }
        }
      }
    });

    socket.on('END_TIME', (payload) => {
      const { roomId, data } = payload;
      let usersInRoomProcess = [];
      for (let key in roomsWithGamesInProcess) {
        if (key === roomId) {
          const usersInRoom = roomsWithGamesInProcess[key];
          usersInRoomProcess = usersInRoom.map(user => {
            for (let key in user) {
              if(user.hasOwnProperty(key)){
                if (key === data.user && user[key] !== null && user[key] >= 0) {
                  user[key] = data.progress;
                }
              }
            }
            return user
          })
          roomsWithGamesInProcess[key] = usersInRoomProcess;
        }
      }
      // check is all users request comeback, this is needed to send the results once
      let processedUsersCount = 0;
      for (let key in roomsWithGamesInProcess) {
        if (key === roomId) {
          const usersInRoom = roomsWithGamesInProcess[key];
          const countUsersInRoom = usersInRoom.length;
          usersInRoomProcess = usersInRoom.map(user => {
            for (let key in user) {
              if(user.hasOwnProperty(key)){
                if (user[key] === null || user[key] < 0) {
                  processedUsersCount++;
                }
              }
            }
          })
          if (processedUsersCount === countUsersInRoom) {
            io.to(roomId).emit('PUBLISHING_RESULTS', {roomId, roomsWithGamesInProcess});
          }
        }
      }
    });

    socket.on('disconnect', () => {
      const index = activeUsers.indexOf(username);
      if (index > -1) {
        activeUsers.splice(index, 1);
      }

    })
  });
};