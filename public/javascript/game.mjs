import { createElement, addClass, removeClass } from "./helpers/domHelper.mjs";
import {callApi} from "./helpers/apiHelper.mjs";
const username = sessionStorage.getItem("username");
if (!username) {
  window.location.replace("/login");
}
const socket = io("", { query: { username } });

socket.on('REGISTRATION_ERROR', (answer) => {
  sessionStorage.clear();
  alert(answer)
  window.location.replace("/login");
})

let currentUser = null;
const setCurrentUser = (user) => {
  currentUser = user;
}

let currentTextToPrint = null;
const setCurrentTextToPrint = (text) => {
  currentTextToPrint = text;
}

let fullTextToPrint = null;
const setFullTextTextToPrint = (text) => {
  fullTextToPrint = text;
}

const getUserProgress = () => {
  const progressWidth = document.querySelector(`.user-progress-${currentUser}`).clientWidth
  const fullWidth = document.querySelector(`.progress-indicator-wrapper`).clientWidth
  if (progressWidth > 0) {
    return progressWidth / fullWidth * -100;
  }
  return null;
}

const checkIsTrueSymbolClicked = (symbol) => {
  if (currentTextToPrint.charAt(0) === symbol) {
    setCurrentTextToPrint(currentTextToPrint.substr(1));
    const numberElementToColor = fullTextToPrint.length - currentTextToPrint.length - 1
    return numberElementToColor;
  } else {
    return undefined;
  }
}

const keyDownListener = (event, roomId) => {
  const isTrueSymbolClicked = checkIsTrueSymbolClicked(event.key);

  if (isTrueSymbolClicked > -1) {
    const textWrapperElement = document.getElementById('text-container')
    addClass(textWrapperElement.childNodes[isTrueSymbolClicked], 'ready-status-green')
    removeClass(textWrapperElement.childNodes[isTrueSymbolClicked], 'next-symbol')
    const userProgressInPCT = ((isTrueSymbolClicked + 1) / fullTextToPrint.length) * 100;

    if (userProgressInPCT < 100) {
      addClass(textWrapperElement.childNodes[isTrueSymbolClicked + 1], 'next-symbol')
      socket.emit('GAME_PROGRESS', {currentUser, userProgressInPCT, roomId})
    } else {
      const finishTime = document.getElementById('seconds-span').innerText
      socket.emit('USER_FINISHED', {currentUser, finishTime, roomId})
      document.removeEventListener('keydown', () => keyDownListener(event, roomId));
    }
  }
}

const createRoomsList = (roomName, usersInRoom) => {
  const mainWrapper = document.querySelector('.rooms-wrapper')
  const secondaryWrapper = createElement({
    tagName: 'div',
    className: 'room'
  });
  const span = createElement({
    tagName: 'span'
  });
  span.innerHTML = `${usersInRoom.length} users connected`
  const h2 = createElement({
    tagName: 'h2'
  });
  h2.innerHTML = roomName;
  const button = createElement({
    tagName: 'button',
    className: 'join-btn',
    attributes: {id: roomName}
  });
  const joinRoom = () => {
    socket.emit('JOIN_ROOM', {roomName, currentUser})
  }
  button.innerHTML = 'Join';
  button.addEventListener('click', joinRoom)
  secondaryWrapper.append(span,h2,button)
  mainWrapper.append(secondaryWrapper)
}

const updateRooms = (payload) => {
  const newMainElement = document.getElementById('rooms-page')
  removeClass (newMainElement, 'display-none');
  const oldMainElement = document.getElementById('game-page')
  addClass(oldMainElement, 'display-none');
  const roomsWrapperElement = document.querySelector('.rooms-wrapper')
  roomsWrapperElement.innerHTML = '';
  for (let key in payload) {
    createRoomsList(key, payload[key]);
  }
}

socket.on('UPDATE_ROOMS', updateRooms)

const onClickCreateRoom = () => {
  const sign = window.prompt("Enter room's name");
  socket.emit('CREATE_ROOM', sign)
}
const createRoomElement = document.getElementById('add-room-btn')
createRoomElement.addEventListener("click", onClickCreateRoom);

socket.on('ROOM_NAME_ERROR', (answer) => {
  alert(answer);
})

socket.on('JOIN_ROOM_ERROR', (answer) => {
  alert(answer)
})

const leaveRoomDone = (payload) => {
  const {roomId, currentUsers, username} = payload;
  const usersInDOM = document.querySelectorAll('.username')
  for (let i = 0; i < usersInDOM.length; ++i) {
    const item = usersInDOM[i];
    if (item.innerText === username) {
      item.closest('.user-info').remove()
    }
  }
}

socket.on('LEAVE_ROOM_DONE', leaveRoomDone)

const addReadyButton = (payload) => {
  const { username, roomId } = payload;
  const rightWrapper = document.querySelector('.right-side')
  rightWrapper.innerHTML = '';
  const onClickReady = () => {
    const userWrapper = document.querySelector(`.user-info-${username}`)
    const readyIndicator = userWrapper.querySelector(`.ready-indicator`)

    if (readyButton.innerText === 'Ready') {
      addClass(readyIndicator, 'ready-status-green');
      removeClass (readyIndicator, 'ready-status-red');
      readyButton.innerText = 'Not Ready';
      socket.emit('READY_USER', {roomId, username, actionType: 'ready'})
    } else {
      addClass(readyIndicator, 'ready-status-red');
      removeClass (readyIndicator, 'ready-status-green');
      readyButton.innerText = 'Ready';
      socket.emit('READY_USER', {roomId, username, actionType: 'notReady'})
    }
  }

  const readyButton = createElement({
    tagName: 'button',
    className: 'join-btn',
    attributes: {
      id: 'ready-btn'
    }
  });
  readyButton.innerHTML = 'Ready';
  readyButton.addEventListener("click", onClickReady);
  rightWrapper.append(readyButton)

}
socket.on('SET_READY_BUTTON', addReadyButton)

const updateRoom = (payload) => {
  const newMainElement = document.getElementById('game-page')
  removeClass(newMainElement, 'display-none')
  const oldMainElement = document.getElementById('rooms-page')
  addClass(oldMainElement, 'display-none')
  const {activeRoomsWithUsers, roomId} = payload;
  let usersInRoom = [];
  for (let key in activeRoomsWithUsers) {
    if (key === roomId) {
      usersInRoom = activeRoomsWithUsers[key];
    }
  }
  const leftWrapper = document.querySelector('.left-side')
  leftWrapper.innerHTML = '';

  const h2 = createElement({
    tagName: 'h2',
    className: 'room-header'
  });
  h2.innerHTML = roomId;

  const onClickBackToRooms = () => {
    socket.emit('BACK_TO_ROOMS', {roomId, currentUser})
  }
  const backButton = createElement({
    tagName: 'button',
    className: 'join-btn',
    attributes: {
      id: `back-rooms`
    }
  });
  backButton.innerHTML = 'Back to rooms';
  backButton.addEventListener("click", onClickBackToRooms);

  leftWrapper.append(h2, backButton)

  usersInRoom.map(user => {
    for (const key in user) {
      const userInfoWrapper = createElement({
        tagName: 'div',
        className: `user-info user-info-${key}`
      });

      const readyIndicator = createElement({
        tagName: 'span',
        className: user[key] === 'ready'
            ? `ready-indicator ready-status-green`
            : `ready-indicator ready-status-red`
      });

      const userNameWrapper = createElement({
        tagName: 'p',
        className: `username username-${key}`
      });
      userNameWrapper.innerHTML = key;
      if (key === currentUser) {
        const youWrapper = createElement({
          tagName: 'span',
          className: 'you',
        });
        youWrapper.innerText = ' (you)'
        userNameWrapper.append(youWrapper);
      }

      const progressIndicatorWrapper = createElement({
        tagName: 'div',
        className: `progress-indicator-wrapper`
      });

      const progressIndicator = createElement({
        tagName: 'span',
        className: `progress-indicator user-progress-${key}`
      });
      progressIndicatorWrapper.append(progressIndicator);
      userInfoWrapper.append(readyIndicator, userNameWrapper, progressIndicatorWrapper)
      leftWrapper.append(userInfoWrapper)
    }
  })

}

socket.on('UPDATE_ROOM', updateRoom)

socket.on('SET_CURRENT_USER', (username) => {
  setCurrentUser(username)
})

const activatedTimer = (payload) => {
  const { seconds, roomId } = payload;

  const backToRoomsElement = document.getElementById(`back-rooms`)
  addClass(backToRoomsElement, 'display-none');
  removeClass(backToRoomsElement, 'join-btn');
  const roomHeaderElement = document.querySelector('.room-header')
  roomHeaderElement.style.marginBottom = '99px';

  const rightWrapper = document.querySelector('.right-side')
  rightWrapper.innerHTML = '';
  rightWrapper.innerText = seconds;

  let timeLeft = seconds;
  const downloadTimer = setInterval(function(){
    timeLeft--;
    rightWrapper.innerText = timeLeft;
    if(timeLeft === 0) {
      clearInterval(downloadTimer);
      socket.emit('START_GAME', {roomId, currentUser});
    }
  },1000);
}

socket.on('ACTIVATED_TIMER', activatedTimer)

const getRandomText = async (payload) => {
  const { roomId, positionIdOfRandomText, seconds } = payload;
  try {
    const randomText = await callApi(`/game/texts/${positionIdOfRandomText}`, 'GET');
    setCurrentTextToPrint(randomText.text);
    setFullTextTextToPrint(randomText.text)
    const rightWrapper = document.querySelector('.right-side')
    rightWrapper.innerHTML = '';
    const textWrapper = createElement({
      tagName: 'div',
      attributes: {
        id: 'text-container'
      }
    });

    const text = randomText.text;
    text.split('').map(symbol => {
      const span = createElement({
        tagName: 'span',
      });
      span.innerText = symbol;
      textWrapper.append(span)
    })
    rightWrapper.append(textWrapper);


    const secondsWrapper = createElement({
      tagName: 'div',
      className: 'seconds-wrapper'
    });

    const spanWithSeconds = createElement({
      tagName: 'span',
      attributes: {
        id: 'seconds-span'
      }
    });
    spanWithSeconds.innerText = seconds;

    const spanWithDescription = createElement({
      tagName: 'span',
    });
    spanWithDescription.innerText = ` seconds left`;

    let timeLeft = seconds;
    const downloadTimer = setInterval(function(){
      timeLeft--;
      spanWithSeconds.innerText = `${timeLeft}`;
      if(timeLeft === 0) {
        clearInterval(downloadTimer);
        const userProgress = getUserProgress();
        socket.emit('END_TIME', {roomId, data: {user:currentUser, progress: userProgress}});
        document.removeEventListener('keydown', () => keyDownListener(event, roomId));
      }
    },1000);
    secondsWrapper.append(spanWithSeconds, spanWithDescription)
    rightWrapper.append(secondsWrapper)
  } catch (e) {
    console.log(e.message)
  }
}

socket.on('REQUEST_FOR_RANDOM_TEXT', getRandomText)

socket.on('KEY_LISTENER', (roomId) => {
  document.addEventListener('keydown', () => keyDownListener(event, roomId));
})

const setUpdatedProgressIndicators = (payload) => {
  const {roomId, roomsWithGamesInProcess} = payload;
  let usersInRoom = [];
  for (let key in roomsWithGamesInProcess) {
    if (key === roomId) {
      usersInRoom = roomsWithGamesInProcess[key];
    }
  }
  usersInRoom.map(user => {
    for (const key in user) {
      const progressIndicatorElement = document.querySelector(`.user-progress-${key}`)
      if(user.hasOwnProperty(key)) {
        progressIndicatorElement.style.width = `${user[key]}%`
        progressIndicatorElement.style.display = `block`
        if (user[key] < 0) {
          progressIndicatorElement.style.width = `100%`
          progressIndicatorElement.style.backgroundColor = 'green'
        }
      }
    }
  })
}

socket.on('UPDATE_PROGRESS_INDICATORS', setUpdatedProgressIndicators)

const setResults = (payload) => {
  const {roomId, roomsWithGamesInProcess} = payload;
  let usersInRoomProcess;
  let userPosition = 0;
  let textInTextarea = '';
  for (let key in roomsWithGamesInProcess) {
    if (key === roomId) {
      const usersInRoom = roomsWithGamesInProcess[key];
      usersInRoomProcess = usersInRoom.map(user => {
        for (let key in user) {
          if(user.hasOwnProperty(key)){
            if (user[key] < 0 || user[key] === null) {
              userPosition++
              textInTextarea += userPosition + '.' + key + '\n'
            }
          }
        }
      })
    }
  }
  document.getElementById('comment-text').value = 'Congrats finishers!\n' + textInTextarea;
}

socket.on('PUBLISHING_RESULTS', setResults)

const sayHelloIntroduce = (username) => {
  document.getElementById('comment-text').value = 'Welcome on our arena \n and introduce new \n member: ' + username;
}

socket.on('SAY_HELLO_INTRODUCE', sayHelloIntroduce)